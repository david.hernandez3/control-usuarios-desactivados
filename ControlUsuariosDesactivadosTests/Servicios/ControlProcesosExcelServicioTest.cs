﻿using ControlUsuariosDesactivados.Servicios;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlUsuariosDesactivadosTests.Servicios
{
    internal class ControlProcesosExcelServicioTest
    {
        [Test]
        public void obtenerCantidadProcesos_returnCantidadProcesos()
        {
            ControlProcesosExcelServicio controlProcesosExcelServicio = new ControlProcesosExcelServicio();
            int cantidadProcesos = controlProcesosExcelServicio.obtenerCantidadProcesos("2127");
            Assert.AreEqual(379, cantidadProcesos);
        }
        [Test]
        public void obtenerCantidadProcesos_returCero()
        {
            ControlProcesosExcelServicio controlProcesosExcelServicio = new ControlProcesosExcelServicio();
            int cantidadProcesos = controlProcesosExcelServicio.obtenerCantidadProcesos("xxxxxxx");
            Assert.AreEqual(0, cantidadProcesos);
        }
        [Test]
        public void obtenerCantidadProcesosTyba_returnCantidadProcesosTyba()
        {
            ControlProcesosExcelServicio controlProcesosExcelServicio = new ControlProcesosExcelServicio();
            int cantidadProcesosTyba = controlProcesosExcelServicio.obtenerCantidadProcesosTyba("2127");
            Assert.AreEqual(230, cantidadProcesosTyba);
        }
        [Test]
        public void obtenerCantidadProcesosTyba_returnCero()
        {
            ControlProcesosExcelServicio controlProcesosExcelServicio = new ControlProcesosExcelServicio();
            int cantidadProcesosTyba = controlProcesosExcelServicio.obtenerCantidadProcesosTyba("xxxx");
            Assert.AreEqual(0, cantidadProcesosTyba);
        }
    }
}
