﻿using ControlUsuariosDesactivados.models;
using ControlUsuariosDesactivados.Servicios;
using GenFu;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlUsuariosDesactivadosTests.Servicios
{
    internal class AbogadosDesactivadosServicioTests
    {
        [Test]
        public void InsertarAbogadosReturnAbogados()
        {
            var abogadosDesactivados = A.ListOf<AbogadoDesactivado>();

            AbogadoDesactivado[] abogadosDesactivadosArrray = new AbogadoDesactivado[abogadosDesactivados.Count];
            for (int i = 0; i < abogadosDesactivados.Count; i++)
            {
                abogadosDesactivadosArrray[i] = abogadosDesactivados[i];
                abogadosDesactivadosArrray[i].Id_particular = null;

            }
            AbogadosDesactivadosServicio abogadosDesactivadosServicio = new AbogadosDesactivadosServicio();           

            Assert.IsNotNull(abogadosDesactivadosServicio.InsertarAbogados(abogadosDesactivadosArrray));
        }
    }
}
