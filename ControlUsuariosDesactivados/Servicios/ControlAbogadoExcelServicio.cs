﻿using ControlUsuariosDesactivados.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlUsuariosDesactivados.Servicios
{
    public class ControlAbogadoExcelServicio
    {
        AbogadosDesactivadosServicio abogadosDesactivadosServicio;
        public ControlAbogadoExcelServicio()
        {
            this.abogadosDesactivadosServicio = new AbogadosDesactivadosServicio();
        }
        public List<AbogadoDesactivado> filtrarPorRangoFechas(List<AbogadoDesactivado> abogadosDesactivados, string? rangoFechaInferior, string? rangoFechaSuperior)
        {
            DateTime rangoFechaInferiorDt = Convert.ToDateTime(rangoFechaInferior);
            DateTime rangoFechaSuperiorDt = Convert.ToDateTime(rangoFechaSuperior);
            List<AbogadoDesactivado> abogadosFiltradosPorFecha = new List<AbogadoDesactivado>();
            for (int i = 0; i < abogadosDesactivados.Count; i++)
            {
                DateTime fechaDeDesactivacion = Convert.ToDateTime(abogadosDesactivados[i].FechaDesactivacion);
                if ((fechaDeDesactivacion >= rangoFechaInferiorDt) && (fechaDeDesactivacion <= rangoFechaSuperiorDt))
                {
                    abogadosFiltradosPorFecha.Add(abogadosDesactivados[i]);
                }
            }
            if (abogadosFiltradosPorFecha.Count > 0)
            {
                return abogadosFiltradosPorFecha;
            }
            List<AbogadoDesactivado> abogadosListaVacia = new List<AbogadoDesactivado>();
            Console.WriteLine("");
            Console.WriteLine("Ningún abogado coincide con el rango de fechas ingresado");
            return abogadosListaVacia;
        }
        public string camcularTiempoDesactivado(string fechaDesactivacion, string fechaReactivacion)
        {
            if (fechaReactivacion == "0000/00/00")
            {
                return "No ha sido activado";
            }
            DateTime fechaDesactivacionDt = Convert.ToDateTime(fechaDesactivacion);
            DateTime fechaReactivacionDt = Convert.ToDateTime(fechaReactivacion);
            TimeSpan diasDiferencia = fechaReactivacionDt.Subtract(fechaDesactivacionDt);
            string diasDeDiferencia = diasDiferencia.Days.ToString() + " dias";
            return diasDeDiferencia;
        }
        public string AbogadoMoroso(string idAbogado)
        {
            int CantidadDeUnIdContenidoEnBdDesactivados = this.abogadosDesactivadosServicio.ObtenerCantidadDeUnIdContenidoEnBdDesactivados(idAbogado);
            if (CantidadDeUnIdContenidoEnBdDesactivados > 2)
            {
                return "Moroso";
            }
            return "No es moroso";
        }

    }
}
