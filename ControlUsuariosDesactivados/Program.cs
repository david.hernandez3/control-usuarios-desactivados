﻿using ControlUsuariosDesactivados.models;
using ControlUsuariosDesactivados.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ControlUsuariosDesactivados
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
            ArchivoServicio archivoServicio = new ArchivoServicio();
            AbogadosDesactivadosServicio abogadosDesactivadosServicio = new AbogadosDesactivadosServicio();
            ControlDesactivadosServicio controlDesactivadosServicio = new ControlDesactivadosServicio();

            //Carga y Guardar usuarios desactivados           
            //string ubicacionArchivo = "..\\..\\..\\Abogados Desactivados.csv";
            string ubicacionArchivo = "..\\Abogados Desactivados.csv";
            AbogadoDesactivado[] abogados = new AbogadoDesactivado[0];          
            abogados = archivoServicio.extraerAbogadosArchivoScv(ubicacionArchivo);
            //archivoServicio.guardarAbogados(abogados);



            //Controla los usuarios desactivados
            //List<AbogadoDesactivado> AbogadosCampoActivoFalseBdSgp = abogadosDesactivadosServicio.buscarAbogadosCampoActivoFalseBdSgp();///1
            //List<Abogado> abogadosfiltradosPorCampoActivoTrue = controlDesactivadosServicio.comprobarCampoActivoBdMonolegal(AbogadosCampoActivoFalseBdSgp, true);
            //List<Abogado> abogadosfiltradosPorCampoActivoFalse = controlDesactivadosServicio.comprobarCampoActivoBdMonolegal(AbogadosCampoActivoFalseBdSgp, false);
            //controlDesactivadosServicio.actualizarBdSgp(abogadosfiltradosPorCampoActivoFalse, "FechaConsulta");
            //controlDesactivadosServicio.insertarAbogadoReactivados(abogadosfiltradosPorCampoActivoTrue);
            //controlDesactivadosServicio.actualizarBdSgp(abogadosfiltradosPorCampoActivoTrue, "Reactivado");

            //Informe control desactivados           
            InformeServicio informeServicio = new InformeServicio();
            //List<AbogadoDesactivado> abogadosDesactivados = abogadosDesactivadosServicio.obtenerAbogados();
            List<AbogadoDesactivado> abogadosDesactivadosList = new List<AbogadoDesactivado>();
            //string pregunta = "¿Quieres filtrar los resultados por fecha de desactivación?";
            //while (abogadosDesactivadosList.Count == 0)
            //{
            //    abogadosDesactivadosList = informeServicio.preguntaFiltrarPorFecha(abogadosDesactivados,pregunta);
            //    pregunta = "¿Aun quieres filtrar los resultados por fecha de desactivación?";
            //}
            //string archivoConFechas = informeServicio.PreguntaArchivoConFechas();
            abogadosDesactivadosList = abogados.ToList();
            informeServicio.crearInforme(abogadosDesactivadosList);

        }

    }
}
